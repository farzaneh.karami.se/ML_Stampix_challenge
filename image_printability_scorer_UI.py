import torch
import torch.nn as nn
import torchvision.transforms as transforms
from PIL import Image
import tkinter as tk
from tkinter import filedialog
import matplotlib.pyplot as plt


# Define the CIFAR-10 model and load its weights (assuming you have a trained model checkpoint)
class CNN(nn.Module):
    def __init__(self):
        super(CNN, self).__init__()
        self.conv = nn.Sequential(
            nn.Conv2d(3, 16, kernel_size=3, stride=1, padding=1),
            nn.MaxPool2d(kernel_size=2, stride=2),
            nn.ReLU(),
            nn.Conv2d(16, 32, kernel_size=3, stride=1, padding=1),
            nn.MaxPool2d(kernel_size=2, stride=2),
            nn.ReLU()
        )
        self.fc = nn.Sequential(
            nn.Linear(32 * 8 * 8, 64),
            nn.ReLU(),
            nn.Linear(64, 10),
        )

    def forward(self, x):
        x = self.conv(x)
        x = x.view(x.size(0), -1)
        x = self.fc(x)
        return x


model = CNN()
model.load_state_dict(torch.load('./model/model.pth'))

# Define transformations for image preprocessing
preprocess = transforms.Compose([
    transforms.Resize((32, 32)),
    transforms.ToTensor(),
    transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
])


# Function to predict the printability score
def predict_printability_score(image_path):
    image = Image.open(image_path)
    image = preprocess(image)
    image = image.unsqueeze(0)  # Add batch dimension

    with torch.no_grad():
        output = model(image)

    # Calculate printability score based on the predicted class
    predicted_class = torch.argmax(output).item()

    # Define class labels for CIFAR-10
    class_labels = [
        "airplane", "automobile", "bird", "cat", "deer", "dog", "frog", "horse", "ship", "truck"
    ]

    # Get the class label and score for the predicted class
    category = class_labels[predicted_class]
    score = output[0][predicted_class].item()

    return category, score


# Function to handle the "Browse" button click
def browse_image():
    file_path = filedialog.askopenfilename(filetypes=[("Image Files", "*.png *.jpg *.jpeg *.gif *.bmp *.ppm *.pgm")])

    if file_path:
        category, score = predict_printability_score(file_path)

        # Update the label text with the category and score
        result_label.config(text=f"Category: {category}\nPrintability Score: {score:.2f}")


# Create the GUI window
root = tk.Tk()
root.title("Image Printability Scorer")

# Create a "Browse" button
browse_button = tk.Button(root, text="Browse Image", command=browse_image)
browse_button.pack(pady=10)

# Create a label to display the category and score
result_label = tk.Label(root, text="", font=("Helvetica", 12))
result_label.pack()

# Start the GUI main loop
root.mainloop()
