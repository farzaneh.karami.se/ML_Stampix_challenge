import torch
import torch.nn as nn
import torch.optim as optim
import torchvision
import torchvision.transforms as transforms
import matplotlib.pyplot as plt
import time

# Define the transformations for data preprocessing
transform = transforms.Compose([
    transforms.ToTensor(),
    transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
])

# Create train and test datasets
train_set = torchvision.datasets.CIFAR10(
    root='./data', train=True, download=True, transform=transform
)
test_set = torchvision.datasets.CIFAR10(
    root='./data', train=False, download=True, transform=transform
)

# Create data loaders
batch_size = 64
num_workers = 2
train_loader = torch.utils.data.DataLoader(train_set, batch_size=batch_size, shuffle=True, num_workers=num_workers)
test_loader = torch.utils.data.DataLoader(test_set, batch_size=batch_size, shuffle=True, num_workers=num_workers)

# Define the CNN model
class CNN(nn.Module):
    def __init__(self):
        super(CNN, self).__init__()
        self.conv = nn.Sequential(
            nn.Conv2d(3, 16, kernel_size=3, stride=1, padding=1),
            nn.MaxPool2d(kernel_size=2, stride=2),
            nn.ReLU(),
            nn.Conv2d(16, 32, kernel_size=3, stride=1, padding=1),
            nn.MaxPool2d(kernel_size=2, stride=2),
            nn.ReLU()
        )
        self.fc = nn.Sequential(
            nn.Linear(32 * 8 * 8, 64),
            nn.ReLU(),
            nn.Linear(64, 10),
        )

    def forward(self, x):
        x = self.conv(x)
        x = x.view(x.size(0), -1)
        x = self.fc(x)
        return x

model = CNN()

# Define loss function and optimizer
criterion = nn.CrossEntropyLoss()
optimizer = optim.SGD(model.parameters(), lr=0.1)

# Training and evaluation functions
def train(model, train_loader, optimizer, criterion, epoch_size):
    loss_list = []
    for epoch in range(epoch_size):
        model.train()
        epoch_loss = 0
        correct = 0
        total = 0
        for images, labels in train_loader:
            optimizer.zero_grad()
            outputs = model(images)
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()
            epoch_loss += loss.item()
            _, predicted = torch.max(outputs.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum().item()
        loss_list.append(epoch_loss / len(train_loader))
        print('Epoch [{}/{}] Training Accuracy: {:.4f}'.format(epoch + 1, epoch_size, correct / total), 'Loss: {:.4f}'.format(loss_list[-1]))
    # Save the model weights to a .pth file
    torch.save(model.state_dict(), './model/model0.pth')
    return loss_list

def test(model, test_loader):
    model.eval()
    correct = 0
    total = 0
    with torch.no_grad():
        for images, labels in test_loader:
            outputs = model(images)
            _, predicted = torch.max(outputs.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum().item()
    accuracy = correct / total
    return accuracy

# Confusion matrix calculation
def calculate_confusion_matrix(model, test_loader):
    model.eval()
    print('Confusion Matrix calculation')
    with torch.no_grad():
        correct = 0
        total = 0
        confusion_matrix = torch.zeros(10, 10)
        for images, labels in test_loader:
            outputs = model(images)
            _, predicted = torch.max(outputs.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum().item()
            for i, j in zip(predicted, labels):
                confusion_matrix[i][j] += 1
        confusion_matrix = confusion_matrix.numpy().astype(int)
        print('confusion_matrix')
        print(confusion_matrix)

        accuracy = (confusion_matrix.diagonal().sum() / confusion_matrix.sum()) * 100
        print(f'Accuracy: {accuracy:.2f}% ')

# Main training and evaluation
epoch_size = 200

def main():
    train_loss_list = train(model, train_loader, optimizer, criterion, epoch_size)
    print('Training took {:.2f} seconds'.format(time.time() - start_time))
    x = range(epoch_size)
    plt.plot(x, train_loss_list)
    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.show()

    test_accuracy = test(model, test_loader)
    print('Test Accuracy: {:.4f}'.format(test_accuracy))
    calculate_confusion_matrix(model, test_loader)

if __name__ == '__main__':
    start_time = time.time()
    main()
